import unittest

from git_tagup.util import gitpython_commit_prefixes, remove_prefix


class TestRemovePrefix(unittest.TestCase):

    def test_remove_prefix(self):
        """Test removal of a list of prefixes from a string."""

        test_string = "some random text."
        prefixed_test_string = gitpython_commit_prefixes[0] + test_string

        result = remove_prefix(prefixed_test_string, gitpython_commit_prefixes)        

        self.assertEqual(result, test_string)


if __name__ == '__main__':
    unittest.main()
